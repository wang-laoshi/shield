package net.quanter.shield.mail.params;

import lombok.Getter;

@Getter
public class MailMessageParam {
    /**
     * 发送者昵称
     */
    private String senderNickname;
    /**
     * 邮件主题
     */
    private String subject;
    /**
     * 邮件内容
     */
    private String content;
    /**
     * 邮件编码格式
     */
    private String encoding;

    public MailMessageParam(String senderNickname, String subject, String content) {
        this.senderNickname = senderNickname;
        this.subject = subject;
        this.content = content;
        this.encoding = "text/html; charset=utf-8";
    }

    public MailMessageParam(String senderNickname, String subject, String content, String encoding) {
        this.senderNickname = senderNickname;
        this.subject = subject;
        this.content = content;
        this.encoding = encoding;
    }
}
