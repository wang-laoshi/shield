package net.quanter.shield.mail.params;


import lombok.Getter;

@Getter
public class MailServerParam {
    /**
     * 用户名
     */
    private final String username;

    /**
     * 密码
     */
    private final String password;

    /**
     * smtp服务主机名
     */
    private final String smtpHost;

    /**
     * smtp服务端口
     */
    private final Integer smtpPort;

    /**
     * 是否开启ssl
     */
    private final boolean isSsl;

    public MailServerParam(String username, String password, String smtpHost, boolean isSsl) {
        this(username, password, smtpHost, 25, isSsl);
    }

    public MailServerParam(String username, String password, String smtpHost) {
        this(username, password, smtpHost, 25, false);
    }

    public MailServerParam(String username, String password, String smtpHost, Integer smtpPort) {
        this(username, password, smtpHost, smtpPort, false);
    }

    public MailServerParam(String username, String password, String smtpHost, Integer smtpPort, boolean isSsl) {
        this.username = username;
        this.password = password;
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.isSsl = isSsl;
    }
}
