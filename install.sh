cd shield-parent
mvn clean install
cd ../shield-common
mvn clean install
cd ../shield-utils
mvn clean install
cd ../shield-mybatis
mvn clean install
cd ../shield-springboot
mvn clean install
cd ../shield-springboot-mybatis
mvn clean install
cd ../shield-dubbo
mvn clean install
cd ../shield-starter/shield-boot-starter
mvn clean install
cd ../shield-cloud-starter
mvn clean install
cd ../..