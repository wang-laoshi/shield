package net.quanter.shield.mq.rocketmq.consumer;


import net.quanter.shield.mq.MQMessageVO;

import java.lang.reflect.Type;

public interface RocketMQPushMessageListener<T> {
    boolean process(MQMessageVO<T> obj);
    Type getMessageType();
}
