package net.quanter.shield.mq.rocketmq.consumer;

import lombok.Getter;
import net.quanter.shield.mq.rocketmq.param.RocketMQTopicParam;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;

@Getter
public class RocketMQConsumerParam {
    final RocketMQTopicParam topic;
    final RocketMQPushMessageListener listener;
    final HashSet<String> tagSet;
    String tagString;

    public RocketMQConsumerParam(
            RocketMQPushMessageListener listener,
            RocketMQTopicParam topicParam, String... tags) {
        this.listener = listener;
        this.topic = topicParam;
        this.tagSet = new HashSet<>();
        if (tags != null) {
            for (String tag : tags) {
                tagSet.add(tag);
            }
        }
        if(tagSet.isEmpty()){
            tagString = "*";
        }else {
            tagString = StringUtils.join(tagSet,"||");
        }
    }

    public boolean containTag(String tag) {
        if (tagSet.isEmpty()) {
            return true;
        } else {
            return tagSet.contains(tag);
        }
    }

    public void setTagString(String tagString){
        this.tagString = tagString;
    }

    public String getTopicAndTagString(){
        String topicName = topic.getName();
        String topicAndTagString = topicName+"_"+getTagString();
        return topicAndTagString;
    }

    @Override
    public String toString() {
        return "[topic=" + topic +
                ", tagSet=" + tagSet +
                "].listener=" + listener.getClass().getName() +
                '}';
    }
}
