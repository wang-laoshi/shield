package net.quanter.shield.mq.rocketmq.enums;

public enum RocketMqType {
    TCP,//商业版TCP版本，高性能，只可内网访问，测试和生产环境用
    HTTP,//商业版HTTP版本，低性能，公网可访问，本地开发用
    Community//社区版
}
