package net.quanter.shield.mq;

import org.apache.rocketmq.client.exception.MQClientException;

public interface MQConsumer {
    /**
     * 启动消息监听
     * @param runType
     */
    void start(RunType runType) throws MQClientException;
    /**
     * 关闭连接
     */
    void stop();
}
