package net.quanter.shield.mq.rocketmq.param;


import lombok.Getter;

@Getter
public class RocketMQTopicParam {

    final String name;
    final String instanceId;

    public RocketMQTopicParam(String topicName, String instanceId) {
        this.name = topicName;
        this.instanceId = instanceId;
    }

    public boolean nameEquals(String name) {
        return this.name != null && this.name.equals(name);
    }
}
