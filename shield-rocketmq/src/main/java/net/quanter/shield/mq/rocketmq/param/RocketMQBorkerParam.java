package net.quanter.shield.mq.rocketmq.param;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public final class RocketMQBorkerParam {
    final String endPoint;
    final String accessId;
    final String accessKey;
}
