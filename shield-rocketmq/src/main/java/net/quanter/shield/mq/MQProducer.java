package net.quanter.shield.mq;

import cn.hutool.core.lang.UUID;
import net.quanter.shield.common.dto.result.ResultDTO;

import java.util.Map;

public interface MQProducer<T, R> {
    String MSG_ID_KEY = "ROCKET_MQ_MSGID";
    ResultDTO errorProducerNull = ResultDTO.failure("producer is null", 3001);

    default MQMessageVO<T> getMQMessage(T t, String tag, String shardKey, String topic, Map<String, String> properties, String messageId) {
        String msgId = null;
        if (messageId == null) {
            msgId = UUID.fastUUID().toString().replace("-", "");
        }else {
            msgId = messageId;
        }
        MQMessageVO<T> MQMessageVO = new MQMessageVO();
        MQMessageVO.setShardKey(shardKey);
        MQMessageVO.setMessageId(msgId);
        MQMessageVO.setTag(tag);
        MQMessageVO.setObj(t);
        MQMessageVO.setTopic(topic);
        MQMessageVO.put(MSG_ID_KEY, msgId);
        if (properties != null) {
            MQMessageVO.putAll(properties);
        }
        MQMessageVO.setRequestId(msgId);
        return MQMessageVO;
    }

    /**
     * 从专用的消息对象反转化成原始消息
     *
     * @param MQMessageVO
     * @return
     */
    R getSourceMessageFromMQMessage(MQMessageVO<T> MQMessageVO);

    /***
     * 发消息
     * @return 专用消息对象
     */
    ResultDTO send(MQMessageVO mqMessageVO, R message);

    /**
     * 关闭连接
     */
    void close();
}
