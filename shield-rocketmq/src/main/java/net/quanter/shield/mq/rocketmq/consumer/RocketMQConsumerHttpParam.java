package net.quanter.shield.mq.rocketmq.consumer;

import com.aliyun.mq.http.MQClient;
import com.aliyun.mq.http.MQConsumer;
import lombok.Getter;
import lombok.Setter;

public class RocketMQConsumerHttpParam {

    @Getter
    RocketMQConsumerParam rocketMQConsumerParam;
    @Getter
    @Setter
    MQConsumer consumer;
    @Getter @Setter
    MQClient mqClient;

    public RocketMQConsumerHttpParam(RocketMQConsumerParam rocketMQConsumerParam) {
        this.rocketMQConsumerParam = rocketMQConsumerParam;
    }
}
