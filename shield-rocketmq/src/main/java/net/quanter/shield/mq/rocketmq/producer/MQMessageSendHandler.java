package net.quanter.shield.mq.rocketmq.producer;


import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.mq.MQMessageVO;

/**
 * 消息发送处理类
 */
public interface MQMessageSendHandler {
    /**
     * 发送消息前
     * @param MQMessageVO 消息对象
     * @return 返回为true代表可以继续发送消息
     */
    boolean beforeSend(MQMessageVO MQMessageVO);
    /**
     * 发送消息后
     * @param sendResult 发送结果
     * @param MQMessageVO 消息对象
     */
    void afterSend(ResultDTO sendResult, MQMessageVO MQMessageVO);
}
