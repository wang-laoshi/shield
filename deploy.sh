mvn clean
cd shield-parent
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-common
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-utils
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-mybatis
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-springboot
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-springboot-mybatis
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-dubbo
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-mail
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-redis
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-rocketmq
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-starter/shield-boot-starter
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-cloud-starter
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../..