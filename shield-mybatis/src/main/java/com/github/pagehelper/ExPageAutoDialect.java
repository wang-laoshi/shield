package com.github.pagehelper;

import com.github.pagehelper.page.PageAutoDialect;
import net.quanter.shield.mybatis.pagehelper.CacheDialect;
import net.quanter.shield.mybatis.pagehelper.SqlServer2000Dialect;

/**
 * 支持cache和sqlserver2000的数据库
 *
 * @author 王老实
 * @since 1.3.12.RELEASE
 */
public class ExPageAutoDialect extends PageAutoDialect {
    static {
        registerDialectAlias("cache", CacheDialect.class);
        registerDialectAlias("sqlserver2000", SqlServer2000Dialect.class);
    }
}
