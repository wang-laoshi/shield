package net.quanter.shield.mybatis.constants;

public class PageConstants {
    public static final String PAGE_NUM = "pageNum";
    public static final String PAGE_SIZE = "pageSIZE";

    public static final int DEFAULT_PAGE_NUM = 1;

    public static final int DEFAULT_PAGE_SIZE = 10;
}
