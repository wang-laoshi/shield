package net.quanter.shield.mybatis.base;


import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * shield-mybatis
 * 父类Entity
 * @author 王老实
 * @param <ID> 主键类型
 * @since 1.3.12.RELEASE
 */
@Entity
public abstract class BaseEntity<ID> {
    @Id
    private ID id;
}
