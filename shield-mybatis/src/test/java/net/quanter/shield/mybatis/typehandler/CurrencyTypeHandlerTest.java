package net.quanter.shield.mybatis.typehandler;

import java.util.Currency;
import java.util.Locale;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/***
 *
 * created on 2021-03-12
 * @author 王老实
 *
 */
class CurrencyTypeHandlerTest {

    @Test
    public void testCurrency() {
        String currencyStr = "USD";
        Currency currency = Currency.getInstance(currencyStr);
        assertEquals(Currency.getInstance(Locale.US), currency);
        assertEquals("US$",currency.getSymbol());
        assertEquals(currencyStr,currency.toString());
    }
}