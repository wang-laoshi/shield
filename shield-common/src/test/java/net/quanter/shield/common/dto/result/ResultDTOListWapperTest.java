package net.quanter.shield.common.dto.result;

import net.quanter.shield.common.dto.result.wapper.ResultDTOListWapper;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
public class ResultDTOListWapperTest {

    @Test
    void testOptional() {
        ResultDTOListWapper<TestClassC> resultDTOListWapper = new ResultDTOListWapper(ResultDTO.success(null));

        assertFalse(resultDTOListWapper.add(new TestClassC("c1")));
        Iterator<TestClassC> iterator = resultDTOListWapper.iterator();
        int i=0;
        while (iterator.hasNext()){
            iterator.next();
            i++;
        }
        assertEquals(0,i);

        ResultDTO<List<TestClassC>> resultDTO = ResultDTOListWapper.emptyList();
        resultDTO.setData(new ArrayList<>());
        resultDTOListWapper = new ResultDTOListWapper(resultDTO);
        assertTrue(resultDTOListWapper.add(new TestClassC("c1")));
        assertEquals(new TestClassC("c1"), resultDTOListWapper.get(0));
        assertEquals(1, resultDTOListWapper.size());
        iterator = resultDTOListWapper.iterator();
        i=0;
        while (iterator.hasNext()){
            assertEquals("c1",iterator.next().cstring);
            i++;
        }
        assertEquals(1,i);
    }

    @Test
    void testListResultDTO() {
        List<TestClassC> list = new ArrayList<>();
        list.add(new TestClassC("c1"));
        list.add(new TestClassC("c2"));
        list.add(new TestClassC("c3"));
        ResultDTO<List<TestClassC>> resultDTO = ResultDTO.success(list);
        ResultDTOListWapper<TestClassC> resultDTOListWapper = new ResultDTOListWapper(resultDTO);
        assertEquals(3, resultDTOListWapper.size());
        resultDTOListWapper.add(new TestClassC("c3"));
        resultDTOListWapper.add(new TestClassC("c3"));
        assertEquals(5, resultDTOListWapper.size());
        assertTrue(resultDTOListWapper.remove(new TestClassC("c3")));
        assertEquals(4, resultDTOListWapper.size());
        List<TestClassC> list1 = Arrays.asList(new TestClassC("c3"));
        assertTrue(resultDTOListWapper.removeAll(list1));
        assertEquals(2, resultDTOListWapper.size());
        assertTrue(resultDTOListWapper.addAll(list1));
        assertEquals(3, resultDTOListWapper.size());
        assertTrue(resultDTOListWapper.contains(new TestClassC("c3")));
        assertTrue(resultDTOListWapper.containsAll(list1));
        resultDTOListWapper.add(new TestClassC("c3"));
        resultDTOListWapper.add(new TestClassC("c3"));
        resultDTOListWapper.removeIf(x->"c3".equals(x.cstring));
        assertEquals(2, resultDTOListWapper.size());
        assertEquals("c1", resultDTOListWapper.get(0).cstring);
        ListIterator<TestClassC> listIterator = resultDTOListWapper.listIterator();
        while (listIterator.hasNext()){
            TestClassC c = listIterator.next();
            c.cstring = c.cstring+"1";
        }
        assertEquals("c11", resultDTOListWapper.get(0).cstring);
        resultDTOListWapper.forEach(x->x.cstring = x.cstring+"1");
        assertEquals("c111", resultDTOListWapper.get(0).cstring);
        resultDTOListWapper.clear();
        assertTrue(resultDTOListWapper.isEmpty());
    }
}
