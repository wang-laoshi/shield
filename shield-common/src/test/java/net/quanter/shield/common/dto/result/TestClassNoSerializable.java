package net.quanter.shield.common.dto.result;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/***
 *
 * @since  2020-09-08
 * @author 王老实
 *
 */
@EqualsAndHashCode(of = "dstring")
public class TestClassNoSerializable {
    public TestClassNoSerializable(String dstring){
        this.dstring = dstring;
    }
    public String dstring;
}
