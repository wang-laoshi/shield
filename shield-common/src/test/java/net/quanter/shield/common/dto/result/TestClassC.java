package net.quanter.shield.common.dto.result;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
@ToString
@EqualsAndHashCode(of = "cstring")
@NoArgsConstructor
@AllArgsConstructor
public class TestClassC implements Serializable {
    public String cstring;
}
