package net.quanter.shield.common.dto.result;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
@ToString
@EqualsAndHashCode
public class TestClassD implements Serializable{
    public String dstring;
}
