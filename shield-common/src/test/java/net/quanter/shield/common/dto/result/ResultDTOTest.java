package net.quanter.shield.common.dto.result;


import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import lombok.EqualsAndHashCode;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/***
 *
 * @since 2020-09-07
 * @author 王老实
 *
 */
@EqualsAndHashCode
class ResultDTOTest {

    @Test
    void testConstructor() {
        ResultDTO<TestClassD> resultDTO = ResultDTO.success();
    }

    @Test
    void testClone() throws IOException, ClassNotFoundException {
        TestClassA a = new TestClassA();
        TestClassB b = new TestClassB();
        a.b = b;
        b.str = "hello";
        a.num = 10;
        TestClassC c1 = new TestClassC();
        c1.cstring = "c1";
        TestClassC c2 = new TestClassC();
        c2.cstring = "c2";
        TestClassD d = new TestClassD();
        d.dstring = "dd";
        b.cList = Arrays.asList(c1, c2);
        ResultDTO<TestClassA> aResultDTO = ResultDTO.success(a);
        String aJson = aResultDTO.toJsonString();
        TypeReference<ResultDTO<TestClassA>> typeReference = new TypeReference<>() {
        };
        ResultDTO<TestClassA> aJsonResult = ResultDTO.parseJson(aJson, typeReference);
        ResultDTO<TestClassA> aJsonResult1 = ResultDTO.parseJson(aJson, typeReference.getType());
        assertEquals(aResultDTO, aJsonResult);
        assertEquals(aResultDTO, aJsonResult1);


        ResultDTO<TestClassA> aResultDTOClone = aResultDTO.clone();
        ResultDTO<TestClassA> aResultDTODeepClone = aJsonResult.deepClone(typeReference);
        assertEquals(aResultDTO, aResultDTOClone);
        assertEquals(aJsonResult, aResultDTOClone);
        assertNotSame(aJsonResult, aResultDTOClone);
        aJsonResult.getData().b.str = "20";
        assertEquals("20", aJsonResult.getData().b.str);
        assertEquals("hello", aResultDTODeepClone.getData().b.str);

        ResultDTO<TestClassD> dResultDTO = ResultDTO.success(d);
        ResultDTO<TestClassD> dResultDTOClone = dResultDTO.clone();
        assertEquals(dResultDTO, dResultDTOClone);
        assertNotSame(dResultDTO, dResultDTOClone);

        TypeReference<ResultDTO<TestClassNoSerializable>> typeReferenceTestClassNoSerializable = new TypeReference<>() {
        };
        ResultDTO<TestClassNoSerializable> noSerializableResultDTO = ResultDTO.success(new TestClassNoSerializable("TestClassNoSerializable"));
        ResultDTO<TestClassNoSerializable> noSerializableResultDTOClone = noSerializableResultDTO.clone();
        assertEquals(noSerializableResultDTO, noSerializableResultDTOClone);
        assertSame(noSerializableResultDTO.getData(), noSerializableResultDTOClone.getData());
        ResultDTO<TestClassNoSerializable> noSerializableResultDTODeepClone = noSerializableResultDTO.deepClone(typeReferenceTestClassNoSerializable);
        assertEquals(noSerializableResultDTO, noSerializableResultDTOClone);
        assertNotSame(noSerializableResultDTO.getData(), noSerializableResultDTODeepClone.getData());
    }

    @Test
    void testHashCodeAndEquals() {
        TestClassA a1 = new TestClassA();
        TestClassA a2 = new TestClassA();
        TestClassA a3 = new TestClassA();
        TestClassB b1 = new TestClassB();
        TestClassB b2 = new TestClassB();
        TestClassB b3 = new TestClassB();
        TestClassC b1c1 = new TestClassC();
        TestClassC b1c2 = new TestClassC();
        TestClassC b2c1 = new TestClassC();
        TestClassC b2c2 = new TestClassC();
        TestClassC b3c1 = new TestClassC();
        TestClassC b3c2 = new TestClassC();
        a1.b = b1;
        a1.num = 10;
        a2.b = b2;
        a2.num = 10;
        a3.b = b3;
        a3.num = 10;
        b1.str = "b";
        b2.str = "b";
        b3.str = "b";
        b1c1.cstring = "bbb";
        b1c2.cstring = "bbb";
        b2c1.cstring = "bbb";
        b2c2.cstring = "bbb";
        b3c1.cstring = "bbb";
        b3c2.cstring = "bbbb";
        b1.cList = Arrays.asList(b1c1, b1c2);
        b2.cList = Arrays.asList(b2c1, b2c2);
        b3.cList = Arrays.asList(b3c1, b3c2);

        assertEquals(a1, a2);
        assertNotEquals(a1, a3);

        ResultDTO<TestClassA> r1 = ResultDTO.success(a1);
        ResultDTO<TestClassA> r2 = ResultDTO.success(a2);
        ResultDTO<TestClassA> r3 = ResultDTO.success(a3);
        JSONObject j1 = new JSONObject();
        j1.put("j1.key", "j1.value");
        r1.x(j1);
        assertEquals(r1, r2);
        assertNotEquals(r1, r3);
        assertNotEquals(r2, r3);

    }
}