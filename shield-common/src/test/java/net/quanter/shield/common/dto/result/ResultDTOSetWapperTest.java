package net.quanter.shield.common.dto.result;

import net.quanter.shield.common.dto.result.wapper.ResultDTOSetWapper;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
public class ResultDTOSetWapperTest {
    @Test
    void testOptionalSet(){
        ResultDTOSetWapper<TestClassC> resultDTOSetWapper = new ResultDTOSetWapper(new ResultDTO<>());
        assertEquals(0, resultDTOSetWapper.stream().count());
        assertEquals(0, resultDTOSetWapper.parallelStream().count());
        assertEquals(0, resultDTOSetWapper.spliterator().estimateSize());
    }

    @Test
    void testSetResultDTO() {
        Set<TestClassC> set = new HashSet<>();
        set.add(new TestClassC("c1"));
        set.add(new TestClassC("c2"));
        set.add(new TestClassC("c3"));
        ResultDTO<Set<TestClassC>> resultDTO = ResultDTO.success(set);
        ResultDTOSetWapper<TestClassC> resultDTOSetWapper = new ResultDTOSetWapper(resultDTO);
        assertEquals(3, resultDTOSetWapper.size());
        resultDTOSetWapper.add(new TestClassC("c3"));
        resultDTOSetWapper.add(new TestClassC("c3"));
        assertEquals(3, resultDTOSetWapper.size());
        assertTrue(resultDTOSetWapper.remove(new TestClassC("c3")));
        assertEquals(2, resultDTOSetWapper.size());
        Set<TestClassC> set1 = new HashSet();
        set1.add(new TestClassC("c3"));
        assertFalse(resultDTOSetWapper.removeAll(set1));
        assertEquals(2, resultDTOSetWapper.size());
        assertTrue(resultDTOSetWapper.addAll(set1));
        assertEquals(3, resultDTOSetWapper.size());
        assertTrue(resultDTOSetWapper.contains(new TestClassC("c3")));
        assertTrue(resultDTOSetWapper.containsAll(set1));
        resultDTOSetWapper.add(new TestClassC("c3"));
        resultDTOSetWapper.add(new TestClassC("c3"));
        resultDTOSetWapper.removeIf(x->"c3".equals(x.cstring));
        assertEquals(2, resultDTOSetWapper.size());
        TestClassC[] testClassCArray = resultDTOSetWapper.toArray(new TestClassC[]{});
        assertEquals(2,testClassCArray.length);
        Iterator<TestClassC> setIterator = resultDTOSetWapper.iterator();
        Set<TestClassC> newSet = new HashSet<>();
        while (setIterator.hasNext()){
            TestClassC c = setIterator.next();
            c.cstring = c.cstring+"1";
            newSet.add(c);
        }
        resultDTOSetWapper.clear();
        resultDTOSetWapper.addAll(newSet);
        assertTrue(resultDTOSetWapper.contains(new TestClassC("c11")));
        newSet = resultDTOSetWapper.stream().peek(x->x.cstring = x.cstring+"1").collect(Collectors.toSet());
        assertTrue(newSet.contains(new TestClassC("c111")));
        resultDTOSetWapper.clear();
        assertTrue(resultDTOSetWapper.isEmpty());
    }
}
