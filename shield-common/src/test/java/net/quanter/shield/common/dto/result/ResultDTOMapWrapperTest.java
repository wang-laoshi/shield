package net.quanter.shield.common.dto.result;

import net.quanter.shield.common.dto.result.wapper.ResultDTOMapWrapper;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
public class ResultDTOMapWrapperTest {
    @Test
    void testMapResultDTO() {
        Map<String, TestClassC> map = new HashMap<>();
        map.put("c1", new TestClassC("c1"));
        map.put("c2", new TestClassC("c2"));
        map.put("c3", new TestClassC("c3"));
        ResultDTO<Map<String, TestClassC>> resultDTO = ResultDTO.success(map);
        ResultDTOMapWrapper<String, TestClassC> resultDTOMapWrapper = new ResultDTOMapWrapper(resultDTO);
        assertEquals(3, resultDTOMapWrapper.size());
        resultDTOMapWrapper.put("c3", new TestClassC("c3"));
        resultDTOMapWrapper.put("c3", new TestClassC("c3"));
        assertEquals(3, resultDTOMapWrapper.size());
        assertEquals(new TestClassC("c3"), resultDTOMapWrapper.replace("c3", new TestClassC("c4")));
        assertEquals(3, resultDTOMapWrapper.size());
        assertEquals(new TestClassC("c4"), resultDTOMapWrapper.remove("c3"));
        assertEquals(2, resultDTOMapWrapper.size());

    }
}
