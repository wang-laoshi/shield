package net.quanter.shield.common.dto.result;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
@ToString
@EqualsAndHashCode
public class TestClassB implements Serializable {
    public String str;
    public List<TestClassC> cList;
}
