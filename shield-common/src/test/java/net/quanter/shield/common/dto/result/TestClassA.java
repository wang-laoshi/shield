package net.quanter.shield.common.dto.result;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/***
 *
 * @(#) 2020-09-08
 * @author 王老实
 *
 */
@ToString
@EqualsAndHashCode
public class TestClassA implements Serializable {
    public Integer num;
    public TestClassB b;
}
