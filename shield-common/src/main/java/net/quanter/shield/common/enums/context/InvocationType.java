package net.quanter.shield.common.enums.context;

/***
 *
 * create on 2020-10-15
 * @author 王老实
 *
 */
public enum  InvocationType {
    LOCAL,
    DUBBO,
    SPRINGCLOUD,
    SPRINGBOOT,
    HESSION,
    NETTY
}
