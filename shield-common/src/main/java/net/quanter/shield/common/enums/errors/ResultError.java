package net.quanter.shield.common.enums.errors;

import net.quanter.shield.common.enums.BaseEnum;

/***
 *
 * created on 2021-03-08
 * @author 王老实
 *
 */
public enum ResultError {

    OBJECT_NOT_SUPPORT_CLONE(2010);
    public int code;
    ResultError(int code) {
        this.code = code;
    }
}
