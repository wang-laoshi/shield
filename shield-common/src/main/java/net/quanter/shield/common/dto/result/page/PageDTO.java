package net.quanter.shield.common.dto.result.page;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author 王老实
 * @since 1.3.12.RELEASE
 */
@Data
@Schema(description="分页类请求")
public class PageDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    public PageDTO() {
        this.pageNum = 1;
        this.pageSize = 10;
        init();
    }

    public PageDTO(Integer pageSize) {
        this.pageNum = 1;
        this.pageSize = pageSize;
        init();
    }

    public PageDTO(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        init();
    }

    private void init() {
        this.startRow = (pageNum - 1) * pageSize;
        this.endRow = pageNum * pageSize;
    }

    @Schema(description = "页码")
    private Integer pageNum;

    @Schema(description = "页面大小")
    private Integer pageSize;

    @Schema(description = "起始行")
    private Integer startRow;

    @Schema(description = "末行")
    private Integer endRow;

    @Schema(description = "总数")
    private Long total;

    @Schema(description = "总页数")
    private Integer pages;

    @Schema(description = "是否包含count查询")
    private Boolean count;

    @Schema(description = "是否分页合理化")
    private Boolean reasonable;
    /**
     * 当设置为true的时候，如果pagesize设置为0（或RowBounds的limit=0），就不执行分页，返回全部结果
     */
    @Schema(description = "是否执行分页")
    private Boolean pageSizeZero;

    @Schema(description = "进行count查询的列名")
    private String countColumn;

    @Schema(description = "排序")
    private String orderBy;

    @Schema(description = "只增加排序")
    private boolean orderByOnly;
}
