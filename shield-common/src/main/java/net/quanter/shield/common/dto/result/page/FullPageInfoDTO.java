package net.quanter.shield.common.dto.result.page;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 *
 * @author 王老实
 * @since 1.3.13.RELEASE
 */
@Schema(description="API分页返回对象完全版")
@Data
@NoArgsConstructor
public class FullPageInfoDTO extends PageInfoDTO {

    @Schema(description = "当前页的数量")
    private Integer size;

    @Schema(description = "起始行数")
    private Long startRow;

    @Schema(description = "结束行数")
    private Long endRow;

    @Schema(description = "总页数")
    private Integer pages;

    @Schema(description = "上一页页码")
    private Integer prePage;

    @Schema(description = "下一页页码")
    private Integer nextPage;

    @Schema(description = "是否为第一页")
    private boolean isFirstPage = false;

    @Schema(description = "是否为最后一页")
    private boolean isLastPage = false;

    @Schema(description = "是否有上一页")
    private boolean hasPreviousPage = false;

    @Schema(description = "是否有下一页")
    private boolean hasNextPage = false;

    @Schema(description = "导航页码数")
    private Integer navigatePages;

    @Schema(description = "所有导航页号")
    private int[] navigatepageNums;

    @Schema(description = "导航条上的第一页")
    private Integer navigateFirstPage;

    @Schema(description = "导航条上的最后一页")
    private Integer navigateLastPage;
}
