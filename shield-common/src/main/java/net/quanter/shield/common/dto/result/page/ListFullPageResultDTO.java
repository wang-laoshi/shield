package net.quanter.shield.common.dto.result.page;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.ToString;
import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.common.enums.http.HttpCode;


/**
 * @author 王老实
 * @since 1.3.12.RELEASE
 */
@Schema(description = "API分页返回对象完整")
@ToString
public class ListFullPageResultDTO<T extends Serializable> extends ResultDTO<List<T>> {

    @Getter
    @Schema(description = "完整分页对象")
    private FullPageInfoDTO page;

    protected ListFullPageResultDTO() {
    }

    public ListFullPageResultDTO(boolean success, String message, Integer code) {
        super(true, message, code, null);
    }

    public ListFullPageResultDTO(List<T> data, FullPageInfoDTO pageInfoDTO) {
        super(true, null, HttpCode.SUCCESS.code, data);
        this.page = pageInfoDTO;
    }

    public ListFullPageResultDTO(boolean success, String message, Integer code, List<T> data) {
        super(success, message, code, data);
    }

    public <N extends Serializable> ListFullPageResultDTO<N> convert(Function<T, N> action) {
        ListFullPageResultDTO<N> newListFullPageResultDTO = new ListFullPageResultDTO(this.isSuccess(), this.getMessage(), this.getCode());
        newListFullPageResultDTO.page = this.page;
        List<N> dataList = this.get()
                .map(x -> x.stream().map(action).collect(Collectors.toList()))
                .orElse(new ArrayList<>());
        newListFullPageResultDTO.setData(dataList);
        return newListFullPageResultDTO;
    }
}
