package net.quanter.shield.common.dto.result.page;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author 王老实
 * @since 1.3.12.RELEASE
 */
@Data
@NoArgsConstructor
@Schema(description="API分页返回对象")
public class PageInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Schema(description = "当前页")
    private Integer pageNum;

    @Schema(description = "每页的数量")
    private Integer pageSize;

    @Schema(description = "总纪录数")
    protected Long total;
}
