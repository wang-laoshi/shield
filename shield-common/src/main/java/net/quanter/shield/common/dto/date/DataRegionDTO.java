package net.quanter.shield.common.dto.date;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/***
 * 该类创建于2020-09-05
 * 时间区间
 *
 * @author 王老实
 * @since 1.3.13.RELEASE
 *
 */
@Schema(description="时间区间类")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataRegionDTO implements Serializable, Cloneable {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    /**
     * 起始时间
     */
    @Schema(description = "起始时间")
    public Date startTime;
    /**
     * 终止时间
     */
    @Schema(description = "终止时间")
    public Date endTime;

    /**
     * 获取时间长度
     *
     * @return startTime到endTime的毫秒数
     */
    public long getDuration() {
        return endTime.getTime() - startTime.getTime();
    }

    private String format(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().format(DATE_FORMAT);
    }

    @Override
    public String toString() {
        return '[' + format(startTime) + ',' + format(endTime) + ']';
    }
}
