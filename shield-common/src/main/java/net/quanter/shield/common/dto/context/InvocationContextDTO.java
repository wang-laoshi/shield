package net.quanter.shield.common.dto.context;

import java.io.Serializable;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/***
 * 方法调用上下文
 * create on 2020-10-15
 * @author 王老实
 *
 */
@Schema(description="方法调用上下文")
@Data
public class InvocationContextDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * traceId
     */
    private String traceId;
    /**
     * index
     */
    private Integer index;

    public Integer addAndGet(int num){
        this.index += num;
        return this.index;
    }

    public Integer setAndGet(int index){
        this.index = index;
        return this.index;
    }
}
