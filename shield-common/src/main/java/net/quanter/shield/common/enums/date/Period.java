package net.quanter.shield.common.enums.date;

/**
 * 时间间隔
 *
 * @author 王老实
 * @since 1.3.12.RELEASE
 */
public enum Period {
    S1(1, "1秒钟"),
    S5(5, "5秒钟"),
    S10(10, "10秒钟"),
    S15(15, "15秒钟"),
    S30(30, "30秒钟"),
    M1(60, "1分钟"),
    M2(120, "2分钟"),
    M3(180, "3分钟"),
    M5(300, "5分钟"),
    M10(600, "10分钟"),
    M15(900, "15分钟"),
    M30(1800, "半小时"),
    H1(3600, "1小时"),
    H2(3600 * 2, "2小时"),
    H4(3600 * 4, "4小时"),
    H6(3600 * 6, "6小时"),
    H12(3600 * 12, "12小时"),
    DAY(86400, "1天"),
    WEEK(86400 * 7, "1周"),
    MONTH(86400 * 30, "1个月"),
    QUARTER(86400 * 90,"1个季度"),
    YEAR(86400 * 365, "1年");
    /**
     * 秒值
     */
    public final int seconds;
    /**
     * 毫秒值
     */
    public final long milliSeconds;
    /**
     * 描述
     */
    public final String desc;

    Period(int seconds, String desc) {
        this.seconds = seconds;
        this.milliSeconds = ((long) seconds) * 1000L;
        this.desc = desc;
    }

    public static final Period contain(String name) {
        for (Period period : Period.values()) {
            if (period.toString().equalsIgnoreCase(name)) {
                return period;
            }
        }
        return null;
    }
}
