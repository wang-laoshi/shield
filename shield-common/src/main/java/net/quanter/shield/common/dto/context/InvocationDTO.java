package net.quanter.shield.common.dto.context;

import java.io.Serializable;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import net.quanter.shield.common.enums.context.InvocationType;

/***
 * 方法调用上下文对象
 * 用于debug调试时起作用
 * 可用在dubbo远程调用，spring远程调用（待实现）
 * created on 2020-10-10
 * @author 王老实
 *
 */
@Schema(description="方法调用上下文返回结果")
@Data
public class InvocationDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 调用id
     */
    @Schema(description = "调用类型")
    private InvocationType type;

    /**
     * 调用顺序
     */
    @Schema(description = "调用顺序")
    private Integer index;

    /**
     * 服务名
     */
    @Schema(description = "服务名")
    private String serviceName;

    /**
     * 方法名
     */
    @Schema(description = "方法名")
    private String methodName;

    /**
     * 参数
     */
    @Schema(description = "参数")
    private Object[] args;

    /**
     * 返回值
     */
    @Schema(description = "返回结果")
    private Object result;

    /**
     * 响应时间
     */
    @Schema(description = "响应时间")
    private Long rt;

    /**
     * 方法调用链传输链
     */
    @Schema(description = "方法调用链传输链")
    private List<InvocationDTO> chain;

    /**
     * 方法里抛出的异常
     */
    @Schema(description = "方法里抛出的异常")
    private Throwable e;

    public InvocationDTO() {
    }

    public void initParams(int length) {
        args = new Object[length];
    }

    public void setParam(Object obj, int index) {
        if (args != null && args.length > index) {
            args[index] = obj;
        }
    }

    public void add(InvocationDTO contextDTO) {
        this.chain.add(contextDTO);
    }
}
