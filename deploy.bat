cd shield-parent
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-common
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-utils
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-mybatis
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-springboot
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-springboot-mybatis
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-dubbo
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-starter/shield-boot-starter
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../shield-nacos-starter
mvn clean install org.apache.maven.plugins:maven-deploy-plugin:2.8:deploy -DskipTests
cd ../..