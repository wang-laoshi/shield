# 神盾局

如果遇到mvn deploy时gpg签名失败的问题，
请执行
`export GPG_TTY=$(tty)`


#### 子项目介绍
+ [shield-parent](#shield-parent)
+ shield-common
+ shield-utils
+ shield-mybatis
+ shield-springboot
+ shield-springboot-mybatis
+ shield-dubbo-mybatis
+ shield-boot-starter
+ shield-cloud-starter
+ shield-jacoco-coverage
+ shield-samples