cd shield-parent
mvn clean deploy -Psonatype
cd ../shield-common
mvn clean deploy -Psonatype
cd ../shield-utils
mvn clean deploy -Psonatype
cd ../shield-mybatis
mvn clean deploy -Psonatype
cd ../shield-springboot
mvn clean deploy -Psonatype
cd ../shield-springboot-mybatis
mvn clean deploy -Psonatype
cd ../shield-dubbo
mvn clean deploy -Psonatype
cd ../shield-starter/shield-boot-starter
mvn clean deploy -Psonatype
cd ../shield-cloud-starter
mvn clean deploy -Psonatype
cd ../..