package net.quanter.shield.springboot.config;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;

/**
 * @author 王老实
 */
@Component("shield-Runtime")
@Slf4j
@ToString
public class Runtime implements Serializable {

    @Value("${spring.profiles.active:dev}")
    @Getter
    String env;

    @Value("${server.servlet.context-path:}")
    @Getter
    String contextPath;

    @Value("${server.port:8080}")
    @Getter
    private int port;

    @Getter
    private String host;

    @Getter
    private int pid;

    @Value("${logging.file.path:}")
    @Getter
    private String loggingFilePath;

    @Getter
    private String indexUrl;

    @Getter
    private String localIp ;

    @PostConstruct
    public void init() {
        try {
            localIp = InetAddress.getLocalHost().getHostAddress();
            String name = ManagementFactory.getRuntimeMXBean().getName();
            String[] names = name.split("@");
            this.pid = Integer.parseInt(names[0]);
            this.host = names[1];
            this.indexUrl  = "http://"+localIp+":"+port+contextPath;
        }catch (Throwable e){
            log.error("解析pid错误",e);
        }
    }
}
