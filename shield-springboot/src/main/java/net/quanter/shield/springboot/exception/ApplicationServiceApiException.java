package net.quanter.shield.springboot.exception;

/**
 * api接口异常
 * 应用接口异常
 * @author 王老实
 * 2019-06-14 10:12
 */
public class ApplicationServiceApiException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public ApplicationServiceApiException(String message) {
        super(message);
    }

    public ApplicationServiceApiException(Throwable cause) {
        super(cause);
    }

    public ApplicationServiceApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationServiceApiException(int code, String message) {
        super(code, message);
    }

    public ApplicationServiceApiException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

}
