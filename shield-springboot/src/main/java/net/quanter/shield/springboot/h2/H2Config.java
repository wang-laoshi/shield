package net.quanter.shield.springboot.h2;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.stereotype.Component;

/***
 *
 * created on 2020-10-23
 * @author 王老实
 *
 */
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnProperty(prefix = "spring.h2.console", name = "enabled", havingValue = "true")
@Component("H2Config")
public class H2Config {
    @Value(value="${spring.h2.console.path:/h2-console}")
    @Getter
    String h2ConsolePath;
}
