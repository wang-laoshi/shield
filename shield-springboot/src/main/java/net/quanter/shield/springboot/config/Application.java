package net.quanter.shield.springboot.config;

import java.io.Serializable;

import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 王老实
 */
@Component("shield-Application")
@ToString
public class Application implements Serializable {
    @Value("${spring.application.name:FROM SHIELD-SPRINGBOOT}")
    @Getter
    String name;
}
