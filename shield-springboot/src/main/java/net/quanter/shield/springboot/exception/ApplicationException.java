package net.quanter.shield.springboot.exception;

import lombok.Data;
import net.quanter.shield.common.enums.http.HttpCode;

/**
 * 应用程序异常
 * @author 王老实
 * 2019-06-17 10:13
 */
@Data
public abstract class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int code = HttpCode.INTERNAL_SERVER_ERROR.code;

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(Throwable cause) {
        super(cause);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(int code, String message) {
        super(message);
        setCode(code);
    }

    public ApplicationException(int code, String message, Throwable cause) {
        super(message, cause);
        setCode(code);
    }
}
