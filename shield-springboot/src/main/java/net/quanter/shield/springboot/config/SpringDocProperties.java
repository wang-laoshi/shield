package net.quanter.shield.springboot.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SpringDocProperties {

    @Value("${springdoc.swagger-ui.enable:false}")
    boolean swaggerUIEnable;

    @Value("${springdoc.api-docs.enable:false}")
    boolean apiDocsEnable;

    @Value("${springdoc.api-docs.path:}")
    @Getter String apiDocsPath;

    public boolean isEnable(){
        return swaggerUIEnable && apiDocsEnable;
    }
}
