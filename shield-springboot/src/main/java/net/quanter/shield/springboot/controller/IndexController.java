package net.quanter.shield.springboot.controller;

import java.io.Serializable;
import java.util.Map;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.common.dto.result.wapper.ResultDTOMapWrapper;
import net.quanter.shield.springboot.config.Application;
import net.quanter.shield.springboot.config.BootConfig;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 默认Controller
 * @author 王老实
 */
@Tag(name = "index-controller", description = "默认Controller")
@RestController
@RequestMapping("")
@Slf4j
public class IndexController {

    @Resource
    Application application;

    @Resource
    BootConfig bootConfig;

    String welcome ;
    public final static String SUCCESS = "success";

    @PostConstruct
    public void init(){
        welcome = application.getName();
    }

    @Operation(summary ="返回应用名")
    @GetMapping("")
    public String index(){
        return welcome;
    }

    @Operation(summary ="返回success字符")
    @GetMapping("/check")
    public String check(){
        return SUCCESS;
    }

    @Operation(summary ="返回应用相关配置信息")
    @GetMapping("/config")
    public ResultDTO<Map<String, Serializable>> config(){
        if(bootConfig.isDebugMode()){
            return bootConfig.getConfigMap();
        }else {
            return ResultDTOMapWrapper.emptyMap();
        }
    }
}
