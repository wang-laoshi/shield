package net.quanter.shield.utils.invocation;


import java.util.ArrayList;
import java.util.List;

import net.quanter.shield.common.dto.context.InvocationContextDTO;
import net.quanter.shield.common.dto.context.InvocationDTO;

/***
 * dubbu线程类
 * created on 2020-10-10
 * @author 王老实
 *
 */
public final class InvocationUtil {
    private static final ThreadLocal<List<InvocationDTO>> invocationThreadLocal = new InheritableThreadLocal<>();
    private static final ThreadLocal<InvocationContextDTO> contextThreadLocal = new InheritableThreadLocal<>();

    public static void begin(InvocationContextDTO contextDTO) {
        invocationThreadLocal.set(new ArrayList<>());
        contextThreadLocal.set(contextDTO);
    }

    public static void end() {
        invocationThreadLocal.remove();
        contextThreadLocal.remove();
    }

    public static InvocationContextDTO getContext(){
        return contextThreadLocal.get();
    }

    public static List<InvocationDTO> getInvocationChain() {
        return invocationThreadLocal.get();
    }

    public static void add(InvocationDTO invocationDTO) {
        if(invocationThreadLocal.get()!=null && invocationDTO!=null){
            invocationThreadLocal.get().add(invocationDTO);
        }
    }

}
