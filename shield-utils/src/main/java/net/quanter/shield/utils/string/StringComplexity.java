package net.quanter.shield.utils.string;

import java.util.Arrays;

/***
 * 字符串复杂度
 * 2020-09-04
 * @author 王老实
 * @since 1.3.12.RELEASE
 *
 */
public enum StringComplexity {
    /*
     * 纯数字
     */
    LEVEL1('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),
    /*
     * 数字+大写字母
     */
    LEVEL2('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'Z', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'),

    /*
     * 数字+大小写字母
     */
    LEVEL3('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        , 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
        , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'Z', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'),

    /*
     * 数字+大小写字母+特殊符号
     */
    LEVEL4('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        , 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
        , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'Z', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        , '_', '-', '@', '*', '.', '%', '&', '#', '=');

    private final char[] chars;

    /**
     * 用default包是不让外包访问
     * @return chars
     */
    char[] getChars() {
        return chars;
    }

    StringComplexity(char... chars) {
        //排序的目的是为了便于在contain方法里使用二分法查找元素
        Arrays.sort(chars);
        this.chars = chars;
    }

    /**
     * 返回默认的枚举值
     * @return LEVEL1
     */
    public static StringComplexity defaultStringComplexity(){
        return LEVEL1;
    }

    public boolean contain(char c){
        return Arrays.binarySearch(chars,c) >= 0;
    }
}
