package net.quanter.shield.utils.date;

import java.time.ZoneId;
import java.util.TimeZone;

/***
 * 时区常量类
 * 2020-09-06
 * @author 王老实
 * @since 1.3.13.RELEASE
 *
 */
public class TimeZones {
    public static final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();
    public static final ZoneId GMT_ZONE_ID = ZoneId.of("GMT");
    public static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");
    public static final ZoneId CHINA_ZONE_ID = ZoneId.of("Asia/Shanghai");
    public static final TimeZone DEFATULT_TIME_ZONE = TimeZone.getDefault();
    public static final TimeZone GMT_TIME_ZONE = TimeZone.getTimeZone(GMT_ZONE_ID);
    public static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone(UTC_ZONE_ID);
    public static final TimeZone CHINA_TIME_ZONE = TimeZone.getTimeZone(CHINA_ZONE_ID);
}
