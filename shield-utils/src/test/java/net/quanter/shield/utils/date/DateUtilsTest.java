package net.quanter.shield.utils.date;

import net.quanter.shield.common.dto.date.DataRegionDTO;
import net.quanter.shield.common.enums.date.Period;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/***
 *
 * 2020-09-05
 * @author 王老实
 *
 */
class DateUtilsTest {

    @Test
    void ceiling() {
        Date d1 = DateFormatter.parseGMT("2020-01-01 01:02:03", DateFormatter.DATE_FORMAT_STANDARD);
        assertEquals(DateFormatter.parseGMT("2020-01-01 02:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.ceiling(d1, Calendar.HOUR));
        assertEquals(DateFormatter.parseChina("2020-01-01 09:03:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.ceiling(d1, Calendar.MINUTE));
        assertEquals(DateFormatter.parseChina("2020-01-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.ceiling(d1, Calendar.DATE));
    }

    @Test
    void getPreEndTime() {
        Date d1 = DateFormatter.parseGMT("2020-01-01 01:02:03", DateFormatter.DATE_FORMAT_STANDARD);
        assertEquals(DateFormatter.parseGMT("2020-01-01 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.truncate(d1, Calendar.HOUR));
        assertEquals(DateFormatter.parseChina("2020-01-01 09:02:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.truncate(d1, Calendar.MINUTE));
        assertEquals(DateFormatter.parseChina("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.truncate(d1, Calendar.DATE));
    }

    @Test
    void getFirstTime() {
        Date date = DateFormatter.parse("2020-12-23 21:53:56.123", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parse("2020-12-23 21:53:56", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S1));
        assertEquals(DateFormatter.parse("2020-12-23 21:53:55", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S5));
        assertEquals(DateFormatter.parse("2020-12-23 21:53:50", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S10));
        assertEquals(DateFormatter.parse("2020-12-23 21:53:45", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S15));
        assertEquals(DateFormatter.parse("2020-12-23 21:53:30", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S30));
        assertEquals(DateFormatter.parse("2020-12-23 21:53:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M1));
        assertEquals(DateFormatter.parse("2020-12-23 21:52:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M2));
        assertEquals(DateFormatter.parse("2020-12-23 21:51:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M3));
        assertEquals(DateFormatter.parse("2020-12-23 21:50:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M5));
        assertEquals(DateFormatter.parse("2020-12-23 21:50:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M10));
        assertEquals(DateFormatter.parse("2020-12-23 21:45:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M15));
        assertEquals(DateFormatter.parse("2020-12-23 21:30:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M30));
        assertEquals(DateFormatter.parse("2020-12-23 21:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H1));
        assertEquals(DateFormatter.parse("2020-12-23 20:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H2));
        assertEquals(DateFormatter.parse("2020-12-23 20:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H4));
        assertEquals(DateFormatter.parse("2020-12-23 18:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H6));
        assertEquals(DateFormatter.parse("2020-12-23 12:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H12));
        assertEquals(DateFormatter.parse("2020-12-23 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.DAY));
        assertEquals(DateFormatter.parse("2020-12-20 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.WEEK));
        assertEquals(DateFormatter.parse("2020-12-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.MONTH));
        assertEquals(DateFormatter.parse("2020-10-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.QUARTER));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.YEAR));

        date = DateFormatter.parse("2020-02-02 01:01:01.123", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parse("2020-02-02 01:01:01", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S1));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S5));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S10));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S15));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S30));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M1));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M2));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M3));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M5));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M10));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M15));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M30));
        assertEquals(DateFormatter.parse("2020-02-02 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H1));
        assertEquals(DateFormatter.parse("2020-02-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H2));
        assertEquals(DateFormatter.parse("2020-02-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H4));
        assertEquals(DateFormatter.parse("2020-02-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H6));
        assertEquals(DateFormatter.parse("2020-02-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H12));
        assertEquals(DateFormatter.parse("2020-02-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.DAY));
        assertEquals(DateFormatter.parse("2020-02-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.WEEK));
        assertEquals(DateFormatter.parse("2020-02-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.MONTH));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.QUARTER));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.YEAR));


        date = DateFormatter.parse("2020-01-01 00:00:00.000", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S1));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S5));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S10));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S15));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.S30));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M1));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M2));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M3));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M5));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M10));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M15));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.M30));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H1));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H2));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H4));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H6));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.H12));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.DAY));
        assertEquals(DateFormatter.parse("2019-12-29 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.WEEK));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.MONTH));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.QUARTER));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriod(date, Period.YEAR));

        date = DateFormatter.parseGMT("2020-01-01 01:01:01", DateFormatter.DATE_FORMAT_STANDARD);
        assertEquals(DateFormatter.parseChina("2020-01-01 09:01:01", DateFormatter.DATE_FORMAT_STANDARD), date);
        assertEquals(DateFormatter.parseGMT("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.DAY));
        assertEquals(DateFormatter.parseChina("2020-01-01 08:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.DAY));
        assertEquals(DateFormatter.parseGMT("2020-01-01 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.H1));
        assertEquals(DateFormatter.parseChina("2020-01-01 09:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.H1));

        date = DateFormatter.parseChina("2020-01-01 01:01:01", DateFormatter.DATE_FORMAT_STANDARD);
        assertEquals(DateFormatter.parseGMT("2019-12-31 17:01:01", DateFormatter.DATE_FORMAT_STANDARD), date);
        assertEquals(DateFormatter.parseGMT("2019-12-31 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.DAY));
        assertEquals(DateFormatter.parseChina("2019-12-31 08:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.DAY));
        assertEquals(DateFormatter.parseGMT("2019-12-31 17:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.H1));
        assertEquals(DateFormatter.parseChina("2020-01-01 01:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getStartTimeByPeriodGMT(date, Period.H1));
    }

    @Test
    void getNextStartTimeByPeriod() {
        Date date = DateFormatter.parse("2020-12-23 21:53:56.123", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parse("2020-12-23 21:53:57", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S1));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S5));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S10));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S15));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S30));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M1));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M2));
        assertEquals(DateFormatter.parse("2020-12-23 21:54:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M3));
        assertEquals(DateFormatter.parse("2020-12-23 21:55:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M5));
        assertEquals(DateFormatter.parse("2020-12-23 22:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M10));
        assertEquals(DateFormatter.parse("2020-12-23 22:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M15));
        assertEquals(DateFormatter.parse("2020-12-23 22:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M30));
        assertEquals(DateFormatter.parse("2020-12-23 22:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H1));
        assertEquals(DateFormatter.parse("2020-12-23 22:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H2));
        assertEquals(DateFormatter.parse("2020-12-24 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H4));
        assertEquals(DateFormatter.parse("2020-12-24 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H6));
        assertEquals(DateFormatter.parse("2020-12-24 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H12));
        assertEquals(DateFormatter.parse("2020-12-24 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.DAY));
        assertEquals(DateFormatter.parse("2020-12-27 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.WEEK));
        assertEquals(DateFormatter.parse("2021-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.MONTH));
        assertEquals(DateFormatter.parse("2021-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.QUARTER));
        assertEquals(DateFormatter.parse("2021-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.YEAR));

        date = DateFormatter.parse("2020-02-02 01:01:01.123", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parse("2020-02-02 01:01:02", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S1));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:05", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S5));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:10", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S10));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:15", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S15));
        assertEquals(DateFormatter.parse("2020-02-02 01:01:30", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S30));
        assertEquals(DateFormatter.parse("2020-02-02 01:02:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M1));
        assertEquals(DateFormatter.parse("2020-02-02 01:02:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M2));
        assertEquals(DateFormatter.parse("2020-02-02 01:03:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M3));
        assertEquals(DateFormatter.parse("2020-02-02 01:05:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M5));
        assertEquals(DateFormatter.parse("2020-02-02 01:10:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M10));
        assertEquals(DateFormatter.parse("2020-02-02 01:15:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M15));
        assertEquals(DateFormatter.parse("2020-02-02 01:30:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M30));
        assertEquals(DateFormatter.parse("2020-02-02 02:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H1));
        assertEquals(DateFormatter.parse("2020-02-02 02:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H2));
        assertEquals(DateFormatter.parse("2020-02-02 04:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H4));
        assertEquals(DateFormatter.parse("2020-02-02 06:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H6));
        assertEquals(DateFormatter.parse("2020-02-02 12:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H12));
        assertEquals(DateFormatter.parse("2020-02-03 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.DAY));
        assertEquals(DateFormatter.parse("2020-02-09 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.WEEK));
        assertEquals(DateFormatter.parse("2020-03-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.MONTH));
        assertEquals(DateFormatter.parse("2020-04-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.QUARTER));
        assertEquals(DateFormatter.parse("2021-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.YEAR));

        date = DateFormatter.parse("2019-12-31 23:59:59.999", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S1));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S5));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S10));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S15));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.S30));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M1));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M2));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M3));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M5));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M10));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M15));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.M30));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H1));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H2));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H4));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H6));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.H12));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.DAY));
        assertEquals(DateFormatter.parse("2020-01-05 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.WEEK));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.MONTH));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.QUARTER));
        assertEquals(DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriod(date, Period.YEAR));

        date = DateFormatter.parseChina("2020-01-01 01:01:01", DateFormatter.DATE_FORMAT_STANDARD);
        assertEquals(DateFormatter.parseGMT("2019-12-31 17:01:01", DateFormatter.DATE_FORMAT_STANDARD), date);
        assertEquals(DateFormatter.parseGMT("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriodGMT(date, Period.DAY));
        assertEquals(DateFormatter.parseChina("2020-01-01 08:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriodGMT(date, Period.DAY));
        assertEquals(DateFormatter.parseGMT("2019-12-31 18:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriodGMT(date, Period.H1));
        assertEquals(DateFormatter.parseChina("2020-01-01 02:00:00", DateFormatter.DATE_FORMAT_STANDARD), DateUtils.getNextStartTimeByPeriodGMT(date, Period.H1));

    }

    @Test
    void getDataRegion() {
        Date date = DateFormatter.parseGMT("2019-12-31 23:59:59.999", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parseChina("2020-01-01 07:59:59.999", DateFormatter.DATE_FORMAT_MS), date);
        DataRegionDTO region = DateUtils.getDataRegionGMT(date, Period.DAY);
        assertEquals(DateFormatter.parseGMT("2019-12-31 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.startTime);
        assertEquals(DateFormatter.parseGMT("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.endTime);
        assertEquals(DateFormatter.parseChina("2019-12-31 08:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.startTime);
        assertEquals(DateFormatter.parseChina("2020-01-01 08:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.endTime);

        date = DateFormatter.parseGMT("2019-12-31 23:59:59.999", DateFormatter.DATE_FORMAT_MS);
        assertEquals(DateFormatter.parseChina("2020-01-01 07:59:59.999", DateFormatter.DATE_FORMAT_MS), date);
        region = DateUtils.getDataRegionChina(date, Period.DAY);
        assertEquals(DateFormatter.parseGMT("2019-12-31 16:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.startTime);
        assertEquals(DateFormatter.parseGMT("2020-01-01 16:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.endTime);
        assertEquals(DateFormatter.parseChina("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.startTime);
        assertEquals(DateFormatter.parseChina("2020-01-02 00:00:00", DateFormatter.DATE_FORMAT_STANDARD), region.endTime);
    }

    @Test
    void getDayRegion() {
        DataRegionDTO d_yestoday_gmt = DateUtils.getYestodayGMT();
        DataRegionDTO d_yestoday_china = DateUtils.getYestodayChina();
        DataRegionDTO d_today_gmt = DateUtils.getTodayGMT();
        DataRegionDTO d_today_china = DateUtils.getTodayChina();
        DataRegionDTO d_tomorrow_gmt = DateUtils.getTomorrowGMT();
        DataRegionDTO d_tomorrow_china = DateUtils.getTomorrowChina();

        assertEquals(Period.DAY.milliSeconds, d_yestoday_gmt.getDuration());
        assertEquals(Period.DAY.milliSeconds, d_yestoday_china.getDuration());
        assertEquals(Period.DAY.milliSeconds, d_today_gmt.getDuration());
        assertEquals(Period.DAY.milliSeconds, d_today_china.getDuration());
        assertEquals(Period.DAY.milliSeconds, d_tomorrow_gmt.getDuration());
        assertEquals(Period.DAY.milliSeconds, d_tomorrow_china.getDuration());


        assertEquals(Period.DAY.milliSeconds, d_yestoday_gmt.endTime.getTime() - d_yestoday_gmt.startTime.getTime());
        assertEquals(Period.DAY.milliSeconds, d_yestoday_china.endTime.getTime() - d_yestoday_china.startTime.getTime());
        assertEquals(Period.DAY.milliSeconds, d_today_gmt.endTime.getTime() - d_yestoday_gmt.endTime.getTime());
        assertEquals(Period.DAY.milliSeconds, d_today_china.endTime.getTime() - d_yestoday_china.endTime.getTime());
        assertEquals(Period.DAY.milliSeconds, d_tomorrow_gmt.endTime.getTime() - d_today_gmt.endTime.getTime());
        assertEquals(Period.DAY.milliSeconds, d_tomorrow_china.endTime.getTime() - d_today_china.endTime.getTime());

        assertEquals(24, DateUtils.getDiff(d_today_gmt.startTime, d_tomorrow_gmt.startTime, Period.H1));
        assertEquals(24, DateUtils.getDiff(d_today_gmt.startTime, d_today_gmt.endTime, Period.H1));
    }


    @Test
    void isWorkDayAndWeekEnd() {
        Date d1 = DateFormatter.parseChina("2020-02-01 07:59:59.999", DateFormatter.DATE_FORMAT_MS);
        assertFalse(DateUtils.isWorkDayChina(d1));
        assertTrue(DateUtils.isWeekEndChina(d1));
        assertTrue(DateUtils.isWorkDayGMT(d1));
        assertFalse(DateUtils.isWeekEndGMT(d1));

        d1 = DateFormatter.parseChina("2020-02-01 08:00:00.000", DateFormatter.DATE_FORMAT_MS);
        assertFalse(DateUtils.isWorkDayChina(d1));
        assertTrue(DateUtils.isWeekEndChina(d1));
        assertFalse(DateUtils.isWorkDayGMT(d1));
        assertTrue(DateUtils.isWeekEndGMT(d1));
    }

    @Test
    void getFirstWorkDayForMonth() {
        Date d1 = DateFormatter.parseChina("2020-02-01 07:59:59.999", DateFormatter.DATE_FORMAT_MS);
        //其实GMT时间 = 2020-01-31 23:59:59
        DataRegionDTO dataRegion = DateUtils.getFirstWorkDayForMonthGMT(d1);
        //期望的GMT时间 = 2020-01-31 23:59:59
        assertEquals(DateFormatter.parseChina("2020-01-01 08:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        assertEquals(DateFormatter.parseChina("2020-01-02 08:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.endTime);


        d1 = DateFormatter.parseChina("2020-02-02 07:59:59.999", DateFormatter.DATE_FORMAT_MS);
        dataRegion = DateUtils.getFirstWorkDayForMonthChina(d1);
        assertEquals(DateFormatter.parseChina("2020-02-03 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        assertEquals(DateFormatter.parseChina("2020-02-04 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.endTime);


        d1 = DateFormatter.parseChina("2020-11-01 00:00:59.999", DateFormatter.DATE_FORMAT_MS);
        dataRegion = DateUtils.getFirstWorkDayForMonthChina(d1);
        assertEquals(DateFormatter.parseChina("2020-11-02 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        assertEquals(DateFormatter.parseChina("2020-11-03 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.endTime);

        d1 = DateFormatter.parseChina("2020-02-02 07:59:59.999", DateFormatter.DATE_FORMAT_MS);
        dataRegion = DateUtils.getFirstWorkDayForMonthGMT(d1);
        assertEquals(DateFormatter.parseChina("2020-02-03 08:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        assertEquals(DateFormatter.parseGMT("2020-02-03 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        dataRegion = DateUtils.getFirstWorkDayForMonthChina(d1);
        assertEquals(DateFormatter.parseChina("2020-02-03 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
    }

    @Test
    void getLastWorkDayByThisMonth() {
        Date d1 = DateFormatter.parseChina("2020-02-01 07:59:59.999", DateFormatter.DATE_FORMAT_MS);
        DataRegionDTO dataRegion = DateUtils.getLastWorkDayForMonthChina(d1);
        assertEquals(DateFormatter.parseChina("2020-02-28 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        assertEquals(DateFormatter.parseChina("2020-02-29 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.endTime);

        d1 = DateFormatter.parseChina("2020-05-31 23:59:59.999", DateFormatter.DATE_FORMAT_MS);
        dataRegion = DateUtils.getLastWorkDayForMonthChina(d1);
        assertEquals(DateFormatter.parseChina("2020-05-29 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.startTime);
        assertEquals(DateFormatter.parseChina("2020-05-30 00:00:00.000", DateFormatter.DATE_FORMAT_MS), dataRegion.endTime);
    }
}