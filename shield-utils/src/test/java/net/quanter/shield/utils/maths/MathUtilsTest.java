package net.quanter.shield.utils.maths;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/***
 *
 * @(#) 2020-09-06
 * @author 王老实
 *
 */
class MathUtilsTest {

    @Test
    void testmax(){
        assertEquals(3.0,MathUtils.max(1.0,2.0,3.0),0.01);
        assertEquals(1.0,MathUtils.min(1.0,2.0,3.0),0.01);
        assertEquals(2.0,MathUtils.avg(new double[]{1.0,2.0,3.0}),0.01);
    }
}
