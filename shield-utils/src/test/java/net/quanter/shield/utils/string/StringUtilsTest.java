package net.quanter.shield.utils.string;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/***
 *
 * 2020-09-04
 * @author 王老实
 *
 */
class StringUtilsTest {

    @Test
    void genString() {
        int length = 10;
        assertGenString(length,StringUtils.genString(length, StringComplexity.LEVEL1),StringComplexity.LEVEL1);
        assertGenString(length,StringUtils.genString(length, StringComplexity.LEVEL2),StringComplexity.LEVEL2);
        assertGenString(length,StringUtils.genString(length, StringComplexity.LEVEL3),StringComplexity.LEVEL3);
        assertGenString(length,StringUtils.genString(length, StringComplexity.LEVEL4),StringComplexity.LEVEL4);
    }

    void assertGenString(int length,String str, StringComplexity complexity){
        assertEquals(length, str.length());
        String s2 = StringUtils.genString(length, complexity);
        for (int i = 0; i < s2.length(); i++) {
            assertTrue(complexity.contain(s2.charAt(i)));
        }
    }

    @Test
    void join() {
        String result = "(11,22,33)";
        String s1 = StringUtils.joinString(new String[]{"11,22,33"}, ",", "(", ")");
        assertEquals(result, s1);

        String s2 = StringUtils.joinString(Lists.newArrayList("11", "22", "33"), ",", "(", ")");
        assertEquals(result, s2);

        String s3 = StringUtils.joinString(new int[]{11, 22, 33}, ',', "(", ")");
        assertEquals(result, s3);

        String s4 = StringUtils.joinString(new char[]{'1', '2', '3'}, ',', "(", ")");
        assertEquals("(1,2,3)",s4);

        String s5 = StringUtils.joinString(new long[]{1L, 2L, 3L}, ',', "(", ")");
        assertEquals("(1,2,3)", s5);

        String s6 = StringUtils.joinString(new double[]{1.1, 2.2, 3.3}, ',', "(", ")");
        assertEquals("(1.1,2.2,3.3)", s6);
    }

    @Test
    void toIntArray(){
        int[] a = StringUtils.stringToIntArray("11,22,33");
        assertEquals(3,a.length);
        assertEquals(11,a[0]);
        assertEquals(22,a[1]);
        assertEquals(33,a[2]);

        long[] b = StringUtils.stringToLongArray("11,22,33");
        assertEquals(3,b.length);
        assertEquals(11,b[0]);
        assertEquals(22,b[1]);
        assertEquals(33,b[2]);

        double[] c = StringUtils.stringToDoubleArray("11.1,22.2,33.3");
        assertEquals(3,c.length);
        assertEquals(11.1,c[0]);
        assertEquals(22.2,c[1]);
        assertEquals(33.3,c[2]);
    }

    @Test
    void humpToLine(){
        String a1 = "AHelloWorldCC";
        String a2 = "_a_hello_world_c_c";
        String b1 = "aHelloWorldCC";
        String b2 = "a_hello_world_c_c";
        String a1Line = StringUtils.humpToLine(a1);
        String a1Hump = StringUtils.lineToHump(a1Line);
        String b1Line = StringUtils.humpToLine(b1);
        String b1Hump = StringUtils.lineToHump(b1Line);
        assertEquals(a2,a1Line);
        assertEquals(a1,a1Hump);
        assertEquals(b2,b1Line);
        assertEquals(b1,b1Hump);
    }

    @Test
    void replaceMiddleWith(){
        assertEquals("1",StringUtils.replaceMiddleWith("1",1,'*'));
        assertEquals("12",StringUtils.replaceMiddleWith("12",1,'*'));
        assertEquals("1*3",StringUtils.replaceMiddleWith("123",1,'*'));
        assertEquals("1*3",StringUtils.replaceMiddleWith("123",2,'*'));
        assertEquals("1*34",StringUtils.replaceMiddleWith("1234",1,'*'));
        assertEquals("1**4",StringUtils.replaceMiddleWith("1234",2,'*'));
        assertEquals("1**45",StringUtils.replaceMiddleWith("12345",2,'*'));
        assertEquals("1***5",StringUtils.replaceMiddleWith("12345",3,'*'));
        assertEquals("1***5",StringUtils.replaceMiddleWith("12345",4,'*'));
        assertEquals("1****6",StringUtils.replaceMiddleWith("123456",4,'*'));
        assertEquals("1****67",StringUtils.replaceMiddleWith("1234567",4,'*'));
        assertEquals("12****78",StringUtils.replaceMiddleWith("12345678",4,'*'));
        assertEquals("12****789",StringUtils.replaceMiddleWith("123456789",4,'*'));
        assertEquals("123****890",StringUtils.replaceMiddleWith("1234567890",4,'*'));
        assertEquals("123****890a",StringUtils.replaceMiddleWith("1234567890a",4,'*'));
        assertEquals("1234****90ab",StringUtils.replaceMiddleWith("1234567890ab",4,'*'));
        assertEquals("1234****90abc",StringUtils.replaceMiddleWith("1234567890abc",4,'*'));
        assertEquals("12345****0abcd",StringUtils.replaceMiddleWith("1234567890abcd",4,'*'));
    }
}