package net.quanter.shield.utils.date;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

/***
 *
 * 2020-08-09
 * @author 王老实
 *
 */
public class DateFormatterTest {

    @Test
    public void testFormatAndParse() {
        Date date = DateFormatter.parseChina("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD);
        Date dateGMT = DateFormatter.parseGMT("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD);
        long time = date.getTime();
        Assertions.assertEquals(1577808000000L, time);
        Assertions.assertEquals("2019-12-31 16:00:00", DateFormatter.formatGMT(date, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2019-12-31 16:00:00", DateFormatter.formatGMT(time, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2019-12-31 16:00:00", DateFormatter.formatUTC(date, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2019-12-31 16:00:00", DateFormatter.formatUTC(time, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2020-01-01 00:00:00", DateFormatter.formatChina(date, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2020-01-01 00:00:00", DateFormatter.formatChina(time, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2020-01-01 00:00:00.000 周三", DateFormatter.formatChina(time, DateFormatter.DATE_FORMAT_FULL));
        Assertions.assertEquals("2020-01-01 00:00:00", DateFormatter.formatChina(date, DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals("2020-01-01", DateFormatter.formatChina(date, DateFormatter.DATE_FORMAT_YMD));
        Assertions.assertEquals("20191231160000", DateFormatter.formatGMT(date, DateFormatter.DATE_FORMAT_STANDARD_SIMPLE));
        Assertions.assertEquals("12-31 16:0:0", DateFormatter.formatGMT(date, DateFormatter.DATE_FORMAT_SHORT));
        Assertions.assertEquals("20191231160000000", DateFormatter.formatGMT(date, DateFormatter.DATE_FORMAT_FULL_SIMPLE));
        Assertions.assertEquals("2019-12-31 16:00:00.000", DateFormatter.formatUTC(time, DateFormatter.DATE_FORMAT_MS));
        Assertions.assertEquals("0101", DateFormatter.format(time, DateFormatter.DATE_FORMAT_MD_SIMPLE));
        Assertions.assertEquals("2019-12-31", DateFormatter.formatUTC(time, DateFormatter.DATE_FORMAT_YMD));
        Assertions.assertEquals("20200101", DateFormatter.formatChina(time, DateFormatter.DATE_FORMAT_YMD_SIMPLE));
        Assertions.assertEquals("202001", DateFormatter.formatChina(time, DateFormatter.DATE_FORMAT_YM_SIMPLE));
        Assertions.assertEquals("1231", DateFormatter.formatGMT(time, DateFormatter.DATE_FORMAT_MD_SIMPLE));
        Assertions.assertEquals("201912311600", DateFormatter.formatGMT(time, DateFormatter.DATE_FORMAT_YMDHM_SIMPLE));
        Assertions.assertEquals("2020-01-01 00:00:00", DateFormatter.format(date));
        Assertions.assertEquals(8, DateFormatter.formatNow(DateFormatter.DATE_FORMAT_YMD_SIMPLE).length());
        Assertions.assertEquals(8, DateFormatter.formatNowGMT(DateFormatter.DATE_FORMAT_YMD_SIMPLE).length());
        Assertions.assertEquals(8, DateFormatter.formatNowChina(DateFormatter.DATE_FORMAT_YMD_SIMPLE).length());
        Assertions.assertEquals(date, DateFormatter.parse("2020-01-01 00:00:00", DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals(date, DateFormatter.parse("20200101", DateFormatter.DATE_FORMAT_YMD_SIMPLE));
        Assertions.assertEquals(date, DateFormatter.parseGMT("2019-12-31 16:00:00", DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals(dateGMT, DateFormatter.parseGMT("20200101", DateFormatter.DATE_FORMAT_YMD_SIMPLE));
        Assertions.assertEquals(date, DateFormatter.parseUTC("2019-12-31 16:00:00", DateFormatter.DATE_FORMAT_STANDARD));
        Assertions.assertEquals(dateGMT, DateFormatter.parseUTC("20200101", DateFormatter.DATE_FORMAT_YMD_SIMPLE));


    }
}