package net.quanter.shield.sample.elasticsearch.dao.repository;

import net.quanter.shield.sample.elasticsearch.dao.entity.EsUser;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface EsUserRepository extends ElasticsearchRepository<EsUser,Long> {
    long deleteESUserByName(String name);
    List<EsUser> queryESUserByName(String name);
}
