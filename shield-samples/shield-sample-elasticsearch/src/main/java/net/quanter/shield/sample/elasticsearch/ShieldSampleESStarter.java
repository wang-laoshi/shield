package net.quanter.shield.sample.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("net.quanter")
public class ShieldSampleESStarter {
    public static void main(String[] args) {
        SpringApplication.run(ShieldSampleESStarter.class);
    }
}
