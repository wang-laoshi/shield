package net.quanter.shield.sample.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/***
 *
 * created on2020-10-22
 * @author 王老实
 *
 */
@ComponentScan("net.quanter")
@SpringBootApplication
public class ShieldSpringBootSampleStarter {
    public static void main(String[] args) {
        SpringApplication.run(ShieldSpringBootSampleStarter.class);
    }
}
