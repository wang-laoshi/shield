package net.quanter.shield.sample.springboot.web.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.sample.springboot.web.vo.HelloVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "hello-controller", description = "HelloController演示类")
@RestController
@RequestMapping("/")
@Slf4j
public class HelloController {

    @Value("${name}")
    String name;
    @Value("${commonfilter}")
    String commonfilter;
    @Value("${envfilter}")
    String envfilter;

    @PostConstruct
    public void init(){
        log.info("HelloController init ,name="+name);
        log.info("commonfilter="+commonfilter);
        log.info("envfilter="+envfilter);
    }

    @GetMapping("/hello")
    public ResultDTO<HelloVO> sayHello(){
        return ResultDTO.success(new HelloVO("name",1));
    }
}
