package net.quanter.shield.sample.springboot.config;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
//        tags = {
//                @Tag(name = "用户管理", description = "用户模块操作"),
//                @Tag(name = "角色管理", description = "角色模块操作")
//        },
        info = @Info(
                title = "springboot-demo项目",
                description = "演示最基础的springboot的基本功能，包括swagger,log4j2,maven-filter,spring.profiles.active等功能",
                version = "1.0.0-SNAPSHOT",
                contact = @Contact(
                        name = " 王老实",
                        email = "custer7572@163.com",
                        url = "https://gitee.com/wang-laoshi/shield"
                ),
                license = @License(
                        name = "Apache 2.0",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"
                )
        ),
//        servers = {
//                @Server(description = "生产环境服务器", url = "https://xxxx.com/api/v1"),
//                @Server(description = "测试环境服务器", url = "https://test.xxxx.com/api/v1")
//        },
        security = @SecurityRequirement(name = "Oauth2"),
        externalDocs = @ExternalDocumentation(
                description = "springdoc参考文献",
                url = "https://springdoc.org/#getting-started")
)
@Configuration
public class SwaggerOpenAPI {
}
