package net.quanter.shield.sample.dubbo.provider.service.impl;

import java.util.Set;

import lombok.NonNull;
import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.common.dto.result.wapper.ResultDTOSetWapper;
import net.quanter.shield.sample.dubbo.dto.DubboSampleProviderRequestDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleProviderResponseDTO;
import net.quanter.shield.sample.dubbo.service.DubboSampleProviderService;
import net.quanter.shield.utils.maths.MathUtils;
import org.apache.dubbo.config.annotation.DubboService;

/***
 * 用@Service注解即可暴露dubbo服务
 * created on 2020-10-05
 * @author 王老实
 *
 */
@DubboService(timeout = 300000)
public class DubboProviderProviderServiceImpl implements DubboSampleProviderService {

    @Override
    public ResultDTO<DubboSampleProviderResponseDTO> call1(@NonNull DubboSampleProviderRequestDTO x) {
        DubboSampleProviderResponseDTO responseDTO = new DubboSampleProviderResponseDTO();
        responseDTO.setStrResult(x.getStr() + "Result1");
        responseDTO.setNumResult(x.getNum() + 11);
        responseDTO.setDoublesResult(MathUtils.sum(x.getDoubles()));
        return ResultDTO.success(responseDTO);
    }

    @Override
    public ResultDTO<DubboSampleProviderResponseDTO> call2(DubboSampleProviderRequestDTO x) {
        DubboSampleProviderResponseDTO responseDTO = new DubboSampleProviderResponseDTO();
        responseDTO.setStrResult(x.getStr() + Integer.parseInt(x.getStr()));
        responseDTO.setNumResult(x.getNum() + 22);
        responseDTO.setDoublesResult(MathUtils.avg(x.getDoubles()));
        return ResultDTO.success(responseDTO);
    }

    @Override
    public ResultDTO<Set<DubboSampleProviderResponseDTO>> call3(@NonNull DubboSampleProviderRequestDTO x) {
        ResultDTOSetWapper<DubboSampleProviderResponseDTO> resultDTOSetWapper = ResultDTOSetWapper.emptySetWapper();
        DubboSampleProviderResponseDTO responseDTO = new DubboSampleProviderResponseDTO();
        responseDTO.setStrResult(x.getStr() + "Result3");
        responseDTO.setNumResult(x.getNum() + 33);
        responseDTO.setDoublesResult(MathUtils.avg(x.getDoubles()));
        resultDTOSetWapper.add(responseDTO);
        return resultDTOSetWapper.getResultDTO();
    }
}
