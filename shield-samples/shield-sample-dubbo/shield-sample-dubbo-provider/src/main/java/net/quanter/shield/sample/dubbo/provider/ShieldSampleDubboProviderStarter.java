package net.quanter.shield.sample.dubbo.provider;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/***
 *
 * created on 2020-09-16
 * @author 王老实
 *
 */
@ComponentScan("net.quanter")
@SpringBootApplication
@EnableDubbo(scanBasePackages = "net.quanter.shield.sample.dubbo.provider.service.impl")
public class ShieldSampleDubboProviderStarter {
    public static void main(String[] args) {
        SpringApplication.run(ShieldSampleDubboProviderStarter.class);
    }
}
