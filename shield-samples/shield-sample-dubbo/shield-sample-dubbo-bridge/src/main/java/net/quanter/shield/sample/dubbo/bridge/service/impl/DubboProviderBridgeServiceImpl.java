package net.quanter.shield.sample.dubbo.bridge.service.impl;

import java.util.Map;
import java.util.Set;

import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.common.dto.result.wapper.ResultDTOMapWrapper;
import net.quanter.shield.sample.dubbo.dto.DubboSampleBridgeRequestDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleBridgeResponseDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleProviderRequestDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleProviderResponseDTO;
import net.quanter.shield.sample.dubbo.service.DubboSampleBridgeService;
import net.quanter.shield.sample.dubbo.service.DubboSampleProviderService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;

/***
 * 用@Service注解即可暴露dubbo服务
 * created on 2020-10-05
 * @author 王老实
 *
 */
@DubboService(timeout = 300000)
public class DubboProviderBridgeServiceImpl implements DubboSampleBridgeService {

    @DubboReference(timeout = 300000)
    DubboSampleProviderService dubboSampleProviderService;

    @Override
    public ResultDTO<Map<String,DubboSampleBridgeResponseDTO>> callBrideg1(DubboSampleBridgeRequestDTO requestDTO) {
        DubboSampleProviderRequestDTO providerRequestDTO = new DubboSampleProviderRequestDTO();
        providerRequestDTO.setNum(requestDTO.getNum());
        providerRequestDTO.setStr(requestDTO.getStr());
        providerRequestDTO.setDoubles(new double[]{requestDTO.getD(),requestDTO.getD()});
        ResultDTO<DubboSampleProviderResponseDTO> result1 = dubboSampleProviderService.call1(providerRequestDTO);
        ResultDTO<DubboSampleProviderResponseDTO> result2 = dubboSampleProviderService.call2(providerRequestDTO);
        ResultDTO<Set<DubboSampleProviderResponseDTO>> result3 = dubboSampleProviderService.call3(providerRequestDTO);
        ResultDTOMapWrapper<String, DubboSampleBridgeResponseDTO> resultDTOMapWrapper = ResultDTOMapWrapper.emptyMapWapper();
        DubboSampleBridgeResponseDTO responseDTO1 = new DubboSampleBridgeResponseDTO();
        responseDTO1.setRequestDTO(requestDTO);
        responseDTO1.setResponseDTO(result1.getData());
        DubboSampleBridgeResponseDTO responseDTO2 = new DubboSampleBridgeResponseDTO();
        responseDTO2.setRequestDTO(requestDTO);
        responseDTO2.setResponseDTO(result2.getData());

        resultDTOMapWrapper.put("result1", responseDTO1);
        resultDTOMapWrapper.put("result2", responseDTO2);
        result3.getData().forEach(x->{
            DubboSampleBridgeResponseDTO responseDTO3 = new DubboSampleBridgeResponseDTO();
            responseDTO3.setRequestDTO(requestDTO);
            responseDTO3.setResponseDTO(x);
            resultDTOMapWrapper.put("result3", responseDTO3);
        });
        return resultDTOMapWrapper.getResultDTO();
    }

    @Override
    public Integer callBrideg2(String name) {
        return name.length();
    }
}
