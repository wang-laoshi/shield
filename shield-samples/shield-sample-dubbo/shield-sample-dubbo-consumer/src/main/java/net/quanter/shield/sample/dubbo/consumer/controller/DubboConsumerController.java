package net.quanter.shield.sample.dubbo.consumer.controller;

import java.util.Map;

import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.common.dto.result.wapper.ResultDTOMapWrapper;
import net.quanter.shield.springboot.annotations.InvocationSupport;
import net.quanter.shield.sample.dubbo.dto.DubboSampleBridgeRequestDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleBridgeResponseDTO;
import net.quanter.shield.sample.dubbo.service.DubboSampleBridgeService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 *
 * created on 2020-10-05
 * @author 王老实
 *
 */
@RestController
public class DubboConsumerController {
    @DubboReference(timeout = 300000,interfaceClass=DubboSampleBridgeService.class)
    DubboSampleBridgeService dubboSampleBridgeService;

    @InvocationSupport
    @GetMapping("/test")
    ResultDTO<DubboSampleBridgeResponseDTO> test(String name) {
        DubboSampleBridgeRequestDTO dubboSampleBridgeRequestDTO = new DubboSampleBridgeRequestDTO();
        dubboSampleBridgeRequestDTO.setStr(name);
        dubboSampleBridgeRequestDTO.setNum(name.length());
        dubboSampleBridgeRequestDTO.setD(name.length() + 1d);
        ResultDTO<Map<String, DubboSampleBridgeResponseDTO>> result2 = dubboSampleBridgeService.callBrideg1(dubboSampleBridgeRequestDTO);
        Integer num = dubboSampleBridgeService.callBrideg2(name);
        ResultDTOMapWrapper<String, DubboSampleBridgeResponseDTO> mapWrapper = new ResultDTOMapWrapper(result2);
        return ResultDTO.success(mapWrapper.get("result2"));
    }
}
