package net.quanter.shield.sample.dubbo.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/***
 *
 * created on 2020-10-11
 * @author 王老实
 *
 */
@Data
public class DubboSampleBridgeResponseDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private DubboSampleBridgeRequestDTO requestDTO;
    private DubboSampleProviderResponseDTO responseDTO;
}
