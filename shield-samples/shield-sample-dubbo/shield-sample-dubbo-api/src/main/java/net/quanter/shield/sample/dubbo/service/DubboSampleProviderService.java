package net.quanter.shield.sample.dubbo.service;

import java.util.Set;

import lombok.NonNull;
import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleProviderRequestDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleProviderResponseDTO;

/***
 *
 * created on 2020-10-05
 * @author 王老实
 *
 */
public interface DubboSampleProviderService {
    ResultDTO<DubboSampleProviderResponseDTO> call1(@NonNull DubboSampleProviderRequestDTO x);
    ResultDTO<DubboSampleProviderResponseDTO> call2(@NonNull DubboSampleProviderRequestDTO x);
    ResultDTO<Set<DubboSampleProviderResponseDTO>> call3(@NonNull DubboSampleProviderRequestDTO x);
}
