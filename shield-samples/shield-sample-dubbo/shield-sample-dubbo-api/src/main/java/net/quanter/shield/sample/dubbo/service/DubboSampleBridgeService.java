package net.quanter.shield.sample.dubbo.service;

import java.util.Map;

import net.quanter.shield.common.dto.result.ResultDTO;
import net.quanter.shield.common.dto.result.wapper.ResultDTOMapWrapper;
import net.quanter.shield.sample.dubbo.dto.DubboSampleBridgeRequestDTO;
import net.quanter.shield.sample.dubbo.dto.DubboSampleBridgeResponseDTO;

/***
 *
 * created on 2020-10-05
 * @author 王老实
 *
 */
public interface DubboSampleBridgeService {
    ResultDTO<Map<String, DubboSampleBridgeResponseDTO>> callBrideg1(DubboSampleBridgeRequestDTO requestDTO);
    Integer callBrideg2(String name);
}
