CREATE TABLE `student`
(
    `id` INT PRIMARY KEY AUTO_INCREMENT COMMENT '主键',
    `name` VARCHAR(20) null,
    `sex` CHAR(1),
    `team_id` INT,
    PRIMARY KEY (`id`)
);

CREATE TABLE `team`
(
    `id` INT PRIMARY KEY AUTO_INCREMENT  COMMENT '主键',
    `name` VARCHAR(20),
    PRIMARY KEY (`id`)
)

