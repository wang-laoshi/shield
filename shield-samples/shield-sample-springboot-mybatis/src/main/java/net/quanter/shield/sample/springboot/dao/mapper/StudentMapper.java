package net.quanter.shield.sample.springboot.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.quanter.shield.sample.springboot.dao.entity.Student;

/***
 *
 * 2020-06-05
 * @author 王老实
 *
 */
public interface StudentMapper extends BaseMapper<Student> {
}
