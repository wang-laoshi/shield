package net.quanter.shield.sample.springboot.controller;

import lombok.*;
import net.quanter.shield.sample.springboot.enums.Sex;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@EqualsAndHashCode
public class StudentDTO implements Serializable {
    private Integer id;
    private String name;
    private Sex sex;
}
