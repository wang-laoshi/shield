package net.quanter.shield.sample.springboot.dao.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import net.quanter.shield.sample.springboot.enums.Sex;

/***
 *
 * created on 2020-10-24
 * @author 王老实
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@EqualsAndHashCode
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String name;
    private Sex sex;
}
