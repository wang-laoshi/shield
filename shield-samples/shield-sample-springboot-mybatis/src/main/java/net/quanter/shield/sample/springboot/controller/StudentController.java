package net.quanter.shield.sample.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.annotation.Resource;
import net.quanter.shield.common.dto.result.page.ListFullPageResultDTO;
import net.quanter.shield.common.dto.result.page.ListPageResultDTO;
import net.quanter.shield.mybatis.page.PageUtil;
import net.quanter.shield.sample.springboot.dao.entity.Student;
import net.quanter.shield.sample.springboot.dao.mapper.StudentMapper;
import net.quanter.shield.springboot.annotations.PageSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * Controller演示类
 * created on 2020-10-24
 * @author 王老实
 *
 */
@Tag(name = "student-controller", description = "Controller演示类")
@RestController
public class StudentController {

    @Resource
    StudentMapper studentMapper;

    /**
     * 测试pageHelper方法分页
     *
     * @param teamId
     * @return
     */
    @Operation(summary = "测试PageHelper分页逻辑")
    @PageSupport
    @GetMapping("/testPageHelper")
    public ListFullPageResultDTO<Student> testPageHelper(
            @Parameter(description = "team的id") Integer teamId) {
        QueryWrapper<Student> studentQueryWrapper = new QueryWrapper();
        ListFullPageResultDTO<Student> listFullPageResultDTO =
                PageUtil.doSelectInfoFull(() -> studentMapper.selectList(studentQueryWrapper.eq("team_id", teamId)));
        return listFullPageResultDTO;
    }

    /**
     * 测试pageHelper方法分页+转换
     *
     * @param teamId
     * @return
     */
    @Operation(summary = "测试PageHelper分页逻辑+数据转换")
    @PageSupport
    @GetMapping("/testPageHelperAndConvert")
    public ListFullPageResultDTO<StudentDTO> testPageHelperAndConvert(
            @Parameter(description = "team的id") Integer teamId) {
        QueryWrapper<Student> studentQueryWrapper = new QueryWrapper();
        ListFullPageResultDTO<Student> listFullPageResultDO =
                PageUtil.doSelectInfoFull(() -> studentMapper.selectList(studentQueryWrapper.eq("team_id", teamId)));
        return listFullPageResultDO.convert(student -> {
            StudentDTO studentDTO = new StudentDTO();
            studentDTO.setId(student.getId());
            studentDTO.setName(student.getName());
//            studentDTO.setSex(student.getSex());
            return studentDTO;
        });
    }

    /**
     * 测试mybatis-plus方法分页
     *
     * @param teamId
     * @return
     */
    @Operation(summary = "测试MybatisPlus分页逻辑")
    @PageSupport
    @GetMapping("/testMybatisPlusPage")
    public ListPageResultDTO<Student> testMybatisPlusPage(
            @Parameter(description = "team的id") Integer teamId) {
        QueryWrapper<Student> studentQueryWrapper = new QueryWrapper();
        studentQueryWrapper.eq("team_id", teamId);
        Page<Student> page = PageUtil.getPage();
        IPage<Student> studentPage = studentMapper.selectPage(page, studentQueryWrapper);
        studentPage.setTotal(studentMapper.selectCount(studentQueryWrapper));
        return PageUtil.getListPageResult(studentPage);
    }
}
