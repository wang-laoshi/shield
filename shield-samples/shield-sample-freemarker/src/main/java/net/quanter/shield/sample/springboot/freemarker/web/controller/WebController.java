package net.quanter.shield.sample.springboot.freemarker.web.controller;


import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import net.quanter.shield.sample.springboot.freemarker.web.vo.HelloVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Tag(name = "hello-controller", description = "HelloController演示类")
@Controller
@RequestMapping("/")
@Slf4j
public class WebController {

    @GetMapping("/hello")
    public String view(String name, Model model) {
        model.addAttribute("name", name);
        List<HelloVO> helloVOList = Lists.newArrayList(
                new HelloVO(name + "a", 1),
                new HelloVO(name + "b", 2),
                new HelloVO(name + "c", 3)
        );
        model.addAttribute("list", helloVOList);
        model.addAttribute("list1", new ArrayList<>());
        model.addAttribute("nameIsNull", name == null);
        return "hello";
    }
}
