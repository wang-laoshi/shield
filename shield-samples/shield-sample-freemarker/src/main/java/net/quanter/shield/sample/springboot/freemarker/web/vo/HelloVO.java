package net.quanter.shield.sample.springboot.freemarker.web.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HelloVO {
    public String name;
    public Integer num;
}
