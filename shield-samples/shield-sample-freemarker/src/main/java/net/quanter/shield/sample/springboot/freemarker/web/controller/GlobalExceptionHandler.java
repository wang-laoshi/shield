package net.quanter.shield.sample.springboot.freemarker.web.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class) // 捕获所有运行时异常
    public String exceptionHandler() {
        return "5xx";
    }
}
