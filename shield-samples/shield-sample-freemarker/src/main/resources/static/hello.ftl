<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>hello</title>
</head>
<body>
    <a href="https://www.jianshu.com/p/c488709d6430">freemarker语法入门</a>
    <p>响应返回的数据>>>>>>>>>>>>>>>>
        <span>${name!}</span>
    </p>
    <p>
        <#if list?size != 0>
            list集合长度:${list?size}
        <#else>
            list集合为空
        </#if>
    </p>
    <p>
        <#if list1?size != 0>
            list1集合长度:${list1?size}
        <#else>
            list1集合为空
        </#if>
    </p>
    <#if list2?size != 0>
    </#if>
    <table border="1">
        <tbody>
        <#list list! as a>
            <tr>
                <td>${a.name!}</td>
                <td>${a.num!}</td>
        </#list>
        </tbody>
    </table>
    <p>
        <#include "copyright.ftl">
    </p>
</body>
</html>