package net.quanter.shield.dubbo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/***
 *
 * created on 2020-10-10
 * @author 王老实
 *
 */
@Configuration
@ImportResource("classpath:dubbo-config.xml")
public class DubboFilterConfig {
}
